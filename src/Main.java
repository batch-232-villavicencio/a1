import com.zuitt.example.Contact;
import com.zuitt.example.Phonebook;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {

        Phonebook phonebook = new Phonebook();
        Contact con1 = new Contact();
        Contact con2 = new Contact();
        con1.setName("John Doe");
        con1.setContactNumber("+639152468596");
        con1.setAddress("Quezon City");
        con2.setName("Jane Doe");
        con2.setContactNumber("+639162148573");
        con2.setAddress("Caloocan City");
        phonebook.checkContents();
        phonebook.setContacts(con1);
        phonebook.setContacts(con2);
        phonebook.checkContents();

    }
}