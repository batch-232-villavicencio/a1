package com.zuitt.example;

import java.util.ArrayList;

public class Phonebook extends Contact{
    private ArrayList<Contact> contacts = new ArrayList<>();

    public Phonebook(){

    }

    public Phonebook(Contact contact){
        this.contacts.add(contact);
    }

    public ArrayList<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(Contact contact) {
        this.contacts.add(contact);
    }

    public void checkContents(){
        if(this.contacts.isEmpty()){
            System.out.println("Phonebook is empty");
        } else {
            this.contacts.forEach(contact ->{
                System.out.println(contact.getName());
                System.out.println("------------------");
                System.out.println(contact.getName() + " has the following registered number:");
                System.out.println(contact.getContactNumber());
                System.out.println(contact.getName() + " has the following registered address:");
                System.out.println("My Home in " + contact.getAddress() + "\n");

            });
        }
    }
}
